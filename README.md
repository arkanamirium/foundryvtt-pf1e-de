# Pathfinder 1e - Deutsche Kompendien

> Deutsche Übersetzungen für die Pathfinder 1e Kompendien

Dies ist eine Weiterführung des Übersetzungsprojekts https://gitlab.com/zenvy/foundryvtt-lang-de-pf1

## Voraussetzungen

* [FoundryVTT](https://foundryvtt.com/)
* [Pathfinder 1e for Foundry VTT](https://gitlab.com/Furyspark/foundryvtt-pathfinder1)
* [Babele](https://foundryvtt.com/packages/babele/)

## Installation

* Setup von FoundryVTT aufrufen
* über den Modulinstaller die Voraussetzungen und dieses Modul installieren
* in der jeweiligen Spielwelt die Module aktivieren

## Features

### Pathfinder 1e System

- [X] Buffs
- [X] Klassen
- [X] Kreaturenarten
- [X] Legendenpfade
- [X] Monsterfähigkeiten
- [X] Regeln
- [X] Rüstungen und Schilde
- [X] Talente
- [X] Technologie (inkl. Roll Tables)
- [X] Völker
- [X] Zauber
- [ ] Gegenstände (in Progress)
- [ ] Klassenfertigkeiten (in Progress)
- [ ] Monster Schablonen
- [ ] Waffen und Munition
- [ ] Roll Tables

### [Pathfinder 1e Content](https://foundryvtt.com/packages/pf-content)

- [ ] Wesenszüge
- [ ] Volksmerkmale

## Unterstützen

Wir sind nicht darauf angewiesen, aber wenn ihr darauf besteht uns finanziell zu unterstützen, dann befüllt gerne die
Tasse / das Glas wieder mit koffeinhaltigen Getränken:

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/F1F0D9KVE)

## Disclaimer

> This FoundryVTT module uses trademarks and/or copyrights owned by Paizo Inc., used under Paizo's Community Use Policy
> (paizo.com/communityuse). We are expressly prohibited from charging you to use or access this content. This FoundryVTT module is not
> published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, visit paizo.com.
